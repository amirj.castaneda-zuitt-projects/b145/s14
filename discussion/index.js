//displays the message 
console.log("Hello from JS");

/*
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
*/

let name = "Juan Dela Cruz";
let mobileNumber = "09065848638";
let greetings;

console.log(greetings);
console.log(name);

// 3 Forms of Data types
// 1. Primitive - string, number, boolean - contains a single value.
// 2. Composite - objects, containes multiple data
// 3. Special - undefined, null


// Data Types
// 1. Strings 
let country = "Philippines";
let province = 'Metro Manila';
// 2. Number 
let headcount = 26;
let planetDistance = 28.3;
// 3. Boolean contains two logical values.
let isMarried = false;
let inGoodConduct = true;
// 4. Null - an assigned value. It means nothing. Intentional
let spouse = null;
let criminalRecords = true;
console.log(spouse);
// 5. Undefined - declared but not defined yet. 
// 6. Objects 
// An array object ex. - basically a list
let grades = [1,2,3,4,5];

let grade1 = {name: 'Hello',
			  id: 12345,
			  employment: 'Software Engineering',
			};

console.log(grades);
console.log(grade1);

// An array should be a collection of data that desrcibes a similar/single topic or subject.
// Use objects(dictionaries in Python) if you want to combine data with different types or subjects.

let cellphone = {
		brand: 'Nokia',
		model: '3310',
		color: 'red',
		features: ["Calling", "Texting", "Ringing"],
		price: 8000
};

console.log(cellphone)

// Variables are declared using the let expression.
// Info stored inside the variable can be changed. They are mutable. 

let personName = "Michael";
console.log(personName);

personName = "HAVA"
console.log(personName);

let pet = "dog";
console.log("this is the initial value of var: " + pet);

// constants are fixed values. Ex. PI
PI = 3.14
console.log(PI);